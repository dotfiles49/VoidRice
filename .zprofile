#!/usr/bin/zsh

# Adds ~/.local/bin to $PATH
export PATH="$PATH:${$(find ~/.local/bin -type d -printf %p:)%%:}"
unsetopt PROMPT_SP

# Defaults
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"

# Dotfile cleanup
export XDG_CONFIG_HOME="$HOME/.config"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
