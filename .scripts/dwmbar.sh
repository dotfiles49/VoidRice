#!/bin/bash
while :; do
	dwm -s "$(sensors | awk '/^Package/ {print $4}' | sed 's/+//') | $(free -h | awk '/^Mem:/ {print $3 "/" $2}') | $(df -h | awk '/^\/dev\/sda/ {print $3 "/" $2}') | $(date "+%a %d/%m/%Y %H:%M") | $(acpi -V | awk '/^Battery\ 0:\ Disch/ {print $4}' | sed 's/,//')"
	sleep 60
done
